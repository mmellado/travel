import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Layout from "../containers/Layout";
import Home from "../containers/Home";

/**
 * Router component. This was techinicaly not necessary, but left it due to the Boilerplate I used
 * Boilerplate used was tiny-react-spa. It's also a project I authored so I'm still the creator of
 * all the codebase.
 */
export default class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Layout>
            <Route exact path="/" component={Home} />
          </Layout>
        </Switch>
      </BrowserRouter>
    );
  }
}
