/* eslint-disable max-len */
import React from "react";

export const MomondoLogo = () => (
  <svg className="c-app_developertest-header-logo" viewBox="0 0 250 38">
    <title>Momondo</title>
    <defs>
      <linearGradient id="c-app_bns-header-logo-blue" x2="0" y2="100%">
        <stop offset="0" stopColor="#00d7e5" />
        <stop offset="1" stopColor="#0066ae" />
      </linearGradient>
      <linearGradient id="c-app_bns-header-logo-pink" x2="0" y2="100%">
        <stop offset="0" stopColor="#ff30ae" />
        <stop offset="1" stopColor="#d1003a" />
      </linearGradient>
      <linearGradient id="c-app_bns-header-logo-orange" x2="0" y2="100%">
        <stop offset="0" stopColor="#ffba00" />
        <stop offset="1" stopColor="#f02e00" />
      </linearGradient>
    </defs>
    <path
      fill="url(#c-app_bns-header-logo-blue)"
      d="M23.2 15.5c2.5-2.7 6-4.4 9.9-4.4 8.7 0 13.4 6 13.4 13.4v12.8c0 .3-.3.5-.5.5h-6c-.3 0-.5-.2-.5-.5V24.5c0-4.6-3.1-5.9-6.4-5.9-3.2 0-6.4 1.3-6.4 5.9v12.8c0 .3-.3.5-.5.5h-5.9c-.3 0-.5-.2-.5-.5V24.5c0-4.6-3.1-5.9-6.4-5.9-3.2 0-6.4 1.3-6.4 5.9v12.8c0 .3-.3.5-.5.5h-6c-.3 0-.5-.2-.5-.5V24.5c0-7.4 4.7-13.4 13.3-13.4 4 0 7.5 1.7 9.9 4.4m54.3 9.1c0 7.5-5.2 13.4-14 13.4s-14-5.9-14-13.4c0-7.6 5.2-13.4 14-13.4 8.8-.1 14 5.9 14 13.4zm-6.7 0c0-3.7-2.4-6.8-7.3-6.8-5.2 0-7.3 3.1-7.3 6.8 0 3.7 2.1 6.8 7.3 6.8 5.1-.1 7.3-3.1 7.3-6.8z"
    />
    <path
      fill="url(#c-app_bns-header-logo-pink)"
      d="M103.8 15.5c2.5-2.7 6-4.4 9.9-4.4 8.7 0 13.4 6 13.4 13.4v12.8c0 .3-.3.5-.5.5h-5.9c-.3 0-.5-.2-.5-.5V24.5c0-4.6-3.1-5.9-6.4-5.9-3.2 0-6.4 1.3-6.4 5.9v12.8c0 .3-.3.5-.5.5H101c-.3 0-.5-.2-.5-.5V24.5c0-4.6-3.1-5.9-6.4-5.9-3.2 0-6.4 1.3-6.4 5.9v12.8c0 .3-.3.5-.5.5h-5.9c-.3 0-.5-.2-.5-.5V24.5c0-7.4 4.7-13.4 13.3-13.4 3.8 0 7.3 1.7 9.7 4.4m54.3 9.1c0 7.5-5.2 13.4-14 13.4s-14-5.9-14-13.4c0-7.6 5.2-13.4 14-13.4 8.7-.1 14 5.9 14 13.4zm-6.7 0c0-3.7-2.3-6.8-7.3-6.8-5.2 0-7.3 3.1-7.3 6.8 0 3.7 2.1 6.8 7.3 6.8 5.1-.1 7.3-3.1 7.3-6.8zm9.8-.1v12.8c0 .3.2.5.5.5h5.9c.3 0 .5-.2.5-.5V24.5c0-4.6 3.1-5.9 6.4-5.9 3.3 0 6.4 1.3 6.4 5.9v12.8c0 .3.2.5.5.5h5.9c.3 0 .5-.2.5-.5V24.5c0-7.4-4.5-13.4-13.4-13.4-8.7 0-13.2 6-13.2 13.4"
    />
    <path
      fill="url(#c-app_bns-header-logo-orange)"
      d="M218.4 0h-5.9c-.3 0-.5.2-.5.5v13c-1.3-1.2-4.3-2.4-7-2.4-8.8 0-14 5.9-14 13.4s5.2 13.4 14 13.4c8.7 0 14-5.2 14-14.6V.4c-.1-.2-.3-.4-.6-.4zm-13.5 31.3c-5.2 0-7.3-3-7.3-6.8 0-3.7 2.1-6.8 7.3-6.8 4.9 0 7.3 3 7.3 6.8s-2.2 6.8-7.3 6.8z"
    />
    <path
      fill="url(#c-app_bns-header-logo-orange)"
      d="M236 11.1c-8.8 0-14 5.9-14 13.4s5.2 13.4 14 13.4 14-5.9 14-13.4c0-7.4-5.3-13.4-14-13.4zm0 20.2c-5.2 0-7.3-3.1-7.3-6.8 0-3.7 2.1-6.8 7.3-6.8 4.9 0 7.3 3.1 7.3 6.8 0 3.8-2.2 6.8-7.3 6.8z"
    />
  </svg>
);

export const Line = () => (
  <svg
    className="c-flights_ticket_route-stops-svg"
    width="100%"
    height="20"
    focusable="false"
  >
    <line
      className="c-flights_ticket_route-stops-svg-line"
      x1="2"
      y1="3"
      x2="100%"
      y2="3"
      strokeWidth="2"
      transform="translate(-1)"
    />
  </svg>
);

export const MomondoWheel = () => (
  <svg
    height="90%"
    viewBox="0 0 20 20"
    width="90%"
    xmlns="http://www.w3.org/2000/svg"
    className="wheel"
  >
    <defs>
      <linearGradient
        gradientUnits="userSpaceOnUse"
        id="b"
        x2="3"
        y1="3"
        y2="3"
      >
        <stop stopColor="#fcb913" offset="0" />
        <stop stopColor="#fbac15" offset=".07" />
        <stop stopColor="#f57a1b" offset=".37" />
        <stop stopColor="#f15620" offset=".64" />
        <stop stopColor="#ef4023" offset=".86" />
        <stop stopColor="#ee3824" offset="1" />
      </linearGradient>

      <linearGradient
        gradientUnits="userSpaceOnUse"
        id="c"
        x2="3"
        y1="3"
        y2="3"
      >
        <stop stopColor="#48c3d6" offset="0" />
        <stop stopColor="#42bbd3" offset=".17" />
        <stop stopColor="#32a6ca" offset=".44" />
        <stop stopColor="#1784bb" offset=".76" />
        <stop stopColor="#0067ae" offset="1" />
      </linearGradient>

      <linearGradient
        gradientUnits="userSpaceOnUse"
        id="d"
        x2="3"
        y1="3"
        y2="3"
      >
        <stop stopColor="#E84698" offset="0" />
        <stop stopColor="#E64392" offset=".18" />
        <stop stopColor="#E23C82" offset=".41" />
        <stop stopColor="#DB3166" offset=".68" />
        <stop stopColor="#D12040" offset=".97" />
        <stop stopColor="#D01F3D" offset="1" />
      </linearGradient>
    </defs>
    <g className="petals">
      <path
        fill="url(#b)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(300 1.57 10)"
      />
      <path
        fill="url(#b)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(330 1.57 10)"
      />
      <path
        fill="url(#b)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(0 1.57 10)"
      />
      <path
        fill="url(#b)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(30 1.57 10)"
      />
      <path
        fill="url(#c)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(60 1.57 10)"
      />
      <path
        fill="url(#c)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(90 1.57 10)"
      />
      <path
        fill="url(#c)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(120 1.57 10)"
      />
      <path
        fill="url(#c)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(150 1.57 10)"
      />
      <path
        fill="url(#d)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(180 1.57 10)"
      />
      <path
        fill="url(#d)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(210 1.57 10)"
      />
      <path
        fill="url(#d)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(240 1.57 10)"
      />
      <path
        fill="url(#d)"
        d="M.123.104C.047.117-.01.187 0 .266l.95 6.142c.006.054.054.1.12.088.347-.046.666-.046 1.01 0 .066 0 .114-.023.118-.09l.948-6.13C3.16.18 3.104.115 3.013.102c-.972-.14-1.95-.136-2.89 0z"
        transform="translate(8.43) rotate(270 1.57 10)"
      />
    </g>
  </svg>
);
