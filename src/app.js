import React, { Component } from "react";
import { Provider } from "react-redux";
import { render } from "react-dom";

import store from "./store/store.js";
import Router from "./router";

import "./scss/main.scss";

/**
 * Tol level component to be embeded on the DOM
 * @return {JSX} The component
 */
class App extends Component {
  render() {
    return store ? (
      <Provider store={store}>
        <Router />
      </Provider>
    ) : (
      <Router />
    );
  }
}

render(<App />, document.getElementById("app"));
