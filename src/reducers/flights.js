import * as actions from "../constants/actions";

const initialState = {
  flights: {}
};

export default function flights(state = initialState, action) {
  switch (action.type) {
    case actions.CLEAR_SEARCH_RESULTS:
      return Object.assign({}, state, initialState);
    case actions.GET_FLIGHTS_RESPONSE:
      return Object.assign({}, state, {
        flights: action.flightsObj
      });
    default:
      return state;
  }
}
