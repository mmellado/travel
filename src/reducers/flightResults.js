import * as actions from "../constants/actions";

const initialState = {
  // fetch status
  isFetching: false,
  fetchKey: null,

  // API results
  offers: [],
  segments: [],
  flights: [],
  legs: [],

  // Results for display
  results: []
};

export default function flightResults(state = initialState, action) {
  switch (action.type) {
    case actions.CLEAR_SEARCH_RESULTS:
      return Object.assign({}, state, initialState);
    case actions.GET_FLIGHTS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        fetchKey: action.fetchKey
      });
    case actions.GET_FLIGHTS_RESPONSE:
      return Object.assign({}, state, {
        isFetching: false,
        results: action.results
      });
    case actions.UPDATE_FLIGHT_RESULTS:
      return Object.assign({}, state, {
        offers: state.offers.concat(action.offers),
        segments: state.segments.concat(action.segments),
        flights: state.flights.concat(action.flights),
        legs: state.legs.concat(action.legs)
      });
    default:
      return state;
  }
}
