import * as actions from "../constants/actions";

const initialState = {
  segments: {}
};

export default function segments(state = initialState, action) {
  switch (action.type) {
    case actions.CLEAR_SEARCH_RESULTS:
      return Object.assign({}, state, initialState);
    case actions.GET_FLIGHTS_RESPONSE:
      return Object.assign({}, state, {
        segments: action.segmentsObj
      });
    default:
      return state;
  }
}
