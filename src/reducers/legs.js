import * as actions from "../constants/actions";

const initialState = {
  legs: {}
};

export default function legs(state = initialState, action) {
  switch (action.type) {
    case actions.CLEAR_SEARCH_RESULTS:
      return Object.assign({}, state, initialState);
    case actions.GET_FLIGHTS_RESPONSE:
      return Object.assign({}, state, {
        legs: action.legsObj
      });
    default:
      return state;
  }
}
