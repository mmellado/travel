import { combineReducers } from "redux";
import flightResults from "./flightResults";
import legs from "./legs";
import segments from "./segments";
import flights from "./flights";

//Import your reducers here

const reducer = combineReducers({
  // Add your reducers to the list
  flightResults,
  legs,
  segments,
  flights
});

export default reducer;
