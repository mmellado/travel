import React, { Component } from "react";
import { MomondoLogo } from "../../constants/svg";

/**
 * Layout component. If the app required multiple pages, this would be present in all of them
 */
export class Layout extends Component {
  /**
   * Render method
   * @return {JSX} The component
   */
  render() {
    return (
      <div id="app-wrapper" className="layout">
        <div id="header" className="layout__header">
          <div className="page-wrapper">
            <h1 id="logo">
              <MomondoLogo />
            </h1>
          </div>
        </div>
        <div id="content" className="layout__content">
          <div className="page-wrapper">{this.props.children}</div>
        </div>
      </div>
    );
  }
}

export default Layout;
