import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import SearchBox from "../../components/SearchBox";
import SearchResults from "../../components/SearchResults";

import { MomondoWheel } from "../../constants/svg";

/**
 * Main component rendering everything
 */
export class Home extends Component {
  static propTypes = {
    flightResults: PropTypes.object
  };

  /**
   * Render method
   * @return {JSX} The component
   */
  render() {
    return (
      <div className="home">
        <SearchBox />
        {this._renderSearchResults()}
      </div>
    );
  }

  /**
   * Handles the logic to render a loader or the retrieved search results
   * NOTE: Loader would be its own component, did it here to save time
   * @return {JSX} The component required to be rendered
   */
  _renderSearchResults = () => {
    const { isFetching, results } = this.props.flightResults;
    return isFetching ? (
      <div className="flights fetching">
        <MomondoWheel />
        <h2>Finding the best flights for you</h2>
      </div>
    ) : (
      <SearchResults results={results} isFetching={isFetching} />
    );
  };
}

/**
 * Maps reducer state variable to the component props
 * @param {Object} state - The reducer state
 */
function mapstateToProps(state) {
  return {
    flightResults: state.flightResults
  };
}

export default connect(mapstateToProps, {})(Home);
