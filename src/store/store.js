import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../reducers";

// use composeEnhancer to be able to leverage the Chrome Redux Extension
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = rootReducer
  ? createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)))
  : null;

export default store;
