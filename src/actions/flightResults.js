import * as actions from "../constants/actions";

import { getRandomString } from "../util/string";
import { fetchJson } from "../util/http";
import {
  buildLegs,
  buildSegments,
  buildFlights,
  buildFinalResults
} from "../util/objectBuilder";

/**
 * Returns the object for dispatching a clearFlightResults request
 * @return {Object} The action object
 */
function clearFlightResultsRequest() {
  return {
    type: actions.CLEAR_SEARCH_RESULTS
  };
}

/**
 * Dispatches the clearFlightResults action
 * @return {Function} The dispatched action
 */
export function clearFlightResults() {
  return dispatch => {
    dispatch(clearFlightResultsRequest());
  };
}

/**
 * Returns the object for dispatching a getFlights request
 * @param {string} fetchKey - Key for the active search
 * @return {Object} The action object
 */
function getFlightsRequest(fetchKey) {
  return {
    type: actions.GET_FLIGHTS_REQUEST,
    fetchKey
  };
}

/**
 * Returns the object for dispatching a getFlights response
 * @param {Object} legsObj - Parsed Legs Object
 * @param {Object} segmentsObj - Parsed Segments Object
 * @param {Object} flightsObj - Parsed Flights Object
 * @param {Array} results - The array of results
 * @return {Object} The action object
 */
function getFlightsResponse(legsObj, segmentsObj, flightsObj, results) {
  return {
    type: actions.GET_FLIGHTS_RESPONSE,
    legsObj,
    segmentsObj,
    flightsObj,
    results
  };
}

/**
 * Returns the object for dispatching a getFlights response
 * @param {Object} flightResults - The response provided by the API call
 * @return {Object} The action object
 */
function updateFlightsRequest(flightResults) {
  return {
    type: actions.UPDATE_FLIGHT_RESULTS,
    offers: flightResults.Offers,
    segments: flightResults.Segments,
    flights: flightResults.Flights,
    legs: flightResults.Legs
  };
}

/**
 * Retrieves the flight results, updating the reducer based on its status
 * @return {Function} The dispatched actions
 */
export function getFlights() {
  return (dispatch, getState) => {
    let { fetchKey, isFetching } = getState().flightResults;
    if (fetchKey) {
      if (!isFetching) {
        fetchKey = getRandomString();
        dispatch(getFlightsRequest(fetchKey));
      }
    } else {
      fetchKey = getRandomString();
      dispatch(getFlightsRequest(fetchKey));
    }

    const fetchUrl = `//cors-anywhere.herokuapp.com/http://momondodevapi.herokuapp.com/api/1.0/FlightSearch/${fetchKey}`;

    fetchJson(fetchUrl).then(res => {
      dispatch(updateFlightsRequest(res));
      if (!res.Done) {
        dispatch(getFlights());
      } else {
        dispatch(parseResults());
      }
    });
  };
}

/**
 * Takes care of building a results array including keys instead of indexes. Then trigger an action
 * to store them in their corresponding reducers, and turn off the isFetching flag
 * @return {Function} Dispatched action
 */
function parseResults() {
  return (dispatch, getState) => {
    const { flights, segments, legs, offers } = getState().flightResults;

    const legsObj = buildLegs(legs);
    const segmentsObj = buildSegments(segments, legs);
    const flightsObj = buildFlights(flights, segments);
    const result = buildFinalResults(offers, flights);
    dispatch(getFlightsResponse(legsObj, segmentsObj, flightsObj, result));
  };
}
