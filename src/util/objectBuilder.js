/**
 * Builds a key based object of legs
 * @param {Array} legs - List of possible legs
 * @return {Object} The legs object
 */
export function buildLegs(legs) {
  const legsObj = {};

  legs.forEach(leg => {
    const key = leg.Key;
    legsObj[key] = leg;
  });

  return legsObj;
}

/**
 * Builds a key based object of segments
 * @param {Array} segments - List of segments
 * @param {Array} legs - List of legs
 * @return {Object} The segments object
 */
export function buildSegments(segments, legs) {
  const segmentsObj = {};

  segments.forEach(segment => {
    const { Key, LegIndexes } = segment;

    segment.legKeys = LegIndexes.map(index => legs[index].Key);
    segmentsObj[Key] = segment;
  });

  return segmentsObj;
}

/**
 * Builds a key based object of Flights
 * @param {Array} flights - List of flights
 * @param {Array} segments - List of segments
 * @return {Object} The segments object
 */
export function buildFlights(flights, segments) {
  const flightsObj = {};

  flights.forEach(flight => {
    const { Key, SegmentIndexes } = flight;

    flight.segmentKeys = SegmentIndexes.map(index => segments[index].Key);
    flightsObj[Key] = flight;
  });

  return flightsObj;
}

/**
 * Builds an array of final results using the flight key instead of an index
 * NOTE: I was already too far in when I realized that a flight key could be the same in different
 * indexes, so I implemented a deduper. With more time I would write logic to prevent dupes from
 * even going into the results array.
 * @param {Array} offers - List of offers
 * @param {Array} flights - List of flights
 * @return {Object} The segments object
 */
export function buildFinalResults(offers, flights) {
  return removeDupeFlights(
    sortResultsByPrice(
      offers.map(offer => {
        const { FlightIndex } = offer;

        offer.flightKey = flights[FlightIndex].Key;
        return offer;
      })
    )
  );
}

/**
 * Sorts a list of
 * @param {Array} results - The list of results to sort
 * @return {Array} The sorted list
 */
function sortResultsByPrice(results) {
  return results.sort((a, b) => a.Price - b.Price);
}

/**
 * Removes dupes from a list of flights
 * @param {Array} flights - The list of flights ot dedupe
 * @return {Array} Deduped list of flights
 */
function removeDupeFlights(flights) {
  const keys = [];
  return flights.filter(flight => {
    if (!keys.includes(flight.flightKey)) {
      keys.push(flight.flightKey);
      return true;
    }
  });
}
