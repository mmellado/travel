/* eslint-disable no-console */
require("es6-promise").polyfill();
require("isomorphic-fetch");

/**
 * Retrieves JSON formatted information from a service
 * @param {string} url - URL to fetch data from
 * @return {Promise} The call's promise attempting to format the result as JSON or the error
 */
export function fetchJson(url) {
  return fetch(url)
    .then(res => res.json())
    .catch(err => {
      console.error(err);
      return err;
    });
}
