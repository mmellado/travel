/**
 * Converts a number to currency format
 * @param {number} price - The price to convert
 * @param {string} currency - The corrency to be appended
 * @return {string} The formatted currency string
 */
export function convertToCurrency(price, currency) {
  const p = price.toFixed(2).split(".");
  p[0] = p[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  return `${p.join(",")} ${currency}`;
}
