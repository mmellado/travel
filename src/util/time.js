/**
 * Builds human readable string of duration
 * @param {number} timeInMinutes - The duration in minutes
 * @return {string} The human readable duration string
 */
export const buildDuration = (timeInMinutes = 0) => {
  let hours = 0;
  let minutes = 0;

  if (!timeInMinutes) {
    return `0h`;
  }

  if (timeInMinutes > 60) {
    hours = Math.ceil(timeInMinutes / 60);
  }

  minutes = Math.ceil(timeInMinutes % 60);

  hours = hours && hours < 10 ? `0${hours}` : hours;
  hours = hours ? `${hours}h` : hours;

  minutes = minutes < 10 ? `0${minutes}` : minutes;
  minutes = minutes ? `${minutes}m` : minutes;

  if (hours && minutes) {
    return `${hours} ${minutes}`;
  } else if (!hours) {
    return minutes;
  } else {
    return hours;
  }
};

/**
 * Turns a timestamp into a human readable hour:minute time
 * @param {string} timestamp - Timestamp to be parsed
 */
export const getTime = timestamp => {
  const date = new Date(timestamp);

  const hours = date.getHours();
  const minutes = `0${date.getMinutes()}`;

  return `${hours}:${minutes.substr(-2)}`;
};
