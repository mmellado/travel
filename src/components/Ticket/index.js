import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { Line } from "../../constants/svg";

import { buildDuration, getTime } from "../../util/time";

/**
 * An one way ticket to be rendered in an individual search result
 * @param {Object} props - The component's props
 * @return {JSX} The component
 */
export const Ticket = props => {
  const { segmentKey, segments, legs } = props;
  const segment = segments[segmentKey];
  const legKeys = segment.legKeys;

  const legList = legKeys.map(key => legs[key]);
  const firstLeg = legList[0];
  const lastLeg = legList[legList.length - 1];

  // Flight info
  const duration = buildDuration(segment.Duration);
  const airline = firstLeg.AirlineName;

  // Origin info
  const departureAirport = firstLeg.OriginIata;
  const departureCity = firstLeg.OriginDisplayName.split(",")[0]
    .split("(")[0]
    .trim();
  const departureTime = getTime(firstLeg.Departure);

  // Destination info
  const arrivalAirport = lastLeg.DestinationIata;
  const arrivalCity = lastLeg.DestinationDisplayName.split(",")[0]
    .split("(")[0]
    .trim();
  const arrivalTime = getTime(firstLeg.Arrival);

  return (
    <div className="ticket flight__ticket">
      <div className="ticket__airline">{airline}</div>
      <div className="ticket__info">
        <div className="ticket__location ticket__location--departure">
          <div className="ticket__location__top">
            <span className="airport">{departureAirport} </span>
            <span className="time">{departureTime}</span>
          </div>
          <div className="ticket__location__bottom">{departureCity}</div>
        </div>
        <div className="ticket__duration">
          {duration}
          <Line />
        </div>
        <div className="ticket__location ticket__location--arrival">
          <div className="ticket__location__top">
            <span className="time">{arrivalTime}</span>
            <span className="airport">{arrivalAirport} </span>
          </div>
          <div className="ticket__location__bottom">{arrivalCity}</div>
        </div>
      </div>
    </div>
  );
};

Ticket.propTypes = {
  segmentKey: PropTypes.string,
  segments: PropTypes.object,
  legs: PropTypes.object
};

/**
 * Maps reducer state to component props
 * @param {Object} state - the reducer state
 */
function mapStateToProps(state) {
  return {
    segments: state.segments.segments,
    legs: state.legs.legs
  };
}

export default connect(mapStateToProps, {})(Ticket);
