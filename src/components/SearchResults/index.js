import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import SearchResult from "../SearchResult";

/**
 * List of search results
 */
export class SearchResults extends Component {
  static propTypes = {
    results: PropTypes.array
  };

  state = {
    isAnimated: false
  };

  /**
   * Lifecycle method
   * Adds a class 100ms after mounting to trigger a render animation
   */
  componentDidMount() {
    setTimeout(() => {
      this.setState({ isAnimated: true });
    }, 100);
  }

  /**
   * Render mehod
   * @return {JSX} The component
   */
  render() {
    const { results } = this.props;
    const { isAnimated } = this.state;
    const flightWrapperclassNames = classNames("flights__wrapper", {
      "flights__wrapper--animated": isAnimated
    });

    return (
      <div className="flights">
        <ul className={flightWrapperclassNames}>
          {results.length
            ? results.map(result => {
                return <SearchResult ticket={result} key={result.flightKey} />;
              })
            : null}
        </ul>
      </div>
    );
  }
}

export default SearchResults;
