import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { getFlights } from "../../actions/flightResults";

/**
 * SearchBox class
 * Simulated search box that provides a CTA to start "searching for flights"
 */
export class SearchBox extends Component {
  static propTypes = {
    flightResults: PropTypes.object,
    getFlights: PropTypes.func
  };

  /**
   * Triggers an action to start fetching the results data from the API
   */
  triggerSearch = () => {
    if (!this.props.flightResults.isFetching) {
      this.props.getFlights();
    }
  };

  /**
   * Render method
   * @return {JSX} The component
   */
  render() {
    const { isFetching } = this.props.flightResults;
    return (
      <div className="search-box">
        <button onClick={this.triggerSearch} disabled={isFetching}>
          Search
        </button>
      </div>
    );
  }
}

/**
 * Maps reducer state variables to the component's props
 * @param {state} state - The reducer state
 */
function mapStateToProps(state) {
  return {
    flightResults: state.flightResults
  };
}

export default connect(mapStateToProps, { getFlights })(SearchBox);
