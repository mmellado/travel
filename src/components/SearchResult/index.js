import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { convertToCurrency } from "../../util/currency";

import Ticket from "../Ticket";

/**
 * An individual search result to be rendered
 * @param {Object} props - The component props
 * @return {JSX} The component
 */
export const SearchResult = props => {
  const { ticket, flights } = props;
  const flight = flights[ticket.flightKey];
  const [toFlight, backFlight] = flight.segmentKeys;
  return (
    <div className="flights__result">
      <div className="flights__result__tickets">
        <Ticket segmentKey={toFlight} />
        <Ticket segmentKey={backFlight} />
      </div>
      <div className="flights__result__actions">
        <div className="flights__result__price">
          {convertToCurrency(ticket.Price, ticket.Currency)}
        </div>

        <a
          href={ticket.Deeplink}
          target="_blank"
          className="flights__result__cta"
        >
          Book
        </a>
      </div>
    </div>
  );
};

SearchResult.propTypes = {
  ticket: PropTypes.object,
  flights: PropTypes.object
};

/**
 * Maps reducer state to component props
 * @param {Object} state - the reducer state
 */
function mapStateToProps(state) {
  return {
    flights: state.flights.flights
  };
}

export default connect(mapStateToProps, {})(SearchResult);
