# Momondo Frontend Challenge

## How to run

1. Clone repository

* `git clone git@bitbucket.org:mmellado/travel.git`

2. Instal dependencies

* With npm
  * `npm install`
* With yarn
  * `yarn install`

3. Run

* Dev server
  * `npm|yarn start`
* Build production files (minified, etc)
  * `npm|yarn run build`

## Notes

The challenge was fun but definitely long. It require me to do some mapping of the API by hand prior to start writing code to have a better understanding of how everyhting fits in together.

One thing that I noticed a bit too late, was that multiple FlightKeyIndexes could contain the same FlightKey. I'm not sure if this was on purpose or it was happening because I missunderstood the API. Since I was already too far in, I decided to implement a deduping function instead of creating a clean array of results from the beginning. That being said, that would clearly be my first optimization if I were to continue to work on this project.

I was also unable to implement tests. I wanted ot have a working project, so I decided to spend more time in building a functioning apllication. Unit tests with Jest and Enzyme would be the next improvement I'd do to the project.

I also went with pure ES6 for the project. I wanted to use TypeScript, but given the learning curve, I decided to optimize time by going with technologies I'm more familiar with. TypeScript would be the next thing I'd change after implementing tests.

One more thing I did was only use part of the static assets provided. CSS is one of the parts that I enjoy working on the most so I wanted to build my own markup and CSS. That being said, due to time constraints I did reuse some of the styles provided in the files provided.

The one thing I was a bit confused by with the API was the way the Indexes work. I made the assumption that the Indexes for the different properties (flight, segment, and leg) were referring to the total number of results (so, the final set of results until Done was true). However, if the case were to be that the Indezes referred exclusively to the results in the particular response made at a single instance of time, the change would be to simply do the parsing immediately after receiving the data when Done is false, and append the results of the following call, simply turning the isFetching flag to off when Done is provided. These were things that again, due to time restrictions, I didn't go deeper into. Having had more time, I'd most likely have done some tests to understand how the API results behaved. Thinking back on the issues I encountered (like duped FlightKeys), I'm assuming these were happening precisely because I missused the API and should've mapped the indexes only with the results of the given call.

To finalize, I can say this challenge was definitely an overwhelming one for an interview challenge. There are a lot of other things I hoped I would've been able to do (like testing, better data handling, animations, accessibility) but couldn't find the time for them. However, despite how overwhelming it was, it was definitely fun to work on and I enjoyed building the application.
